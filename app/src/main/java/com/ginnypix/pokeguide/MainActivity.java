package com.ginnypix.pokeguide;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends AppCompatActivity {

    InterstitialAd mInterstitialAd;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // getActivity() will hand over the context to the method
        // if you call this inside an activity, simply replace getActivity() by "this"
        if (!isConnected(this)) buildDialog(this).show();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3646884322303537/5205997602");

        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();

                Log.i("NAB", "onAdClosed");

                startActivity(intent);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);

                Log.i("NAB", "onAdFailedToLoad");
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();

                Log.i("NAB", "onAdLeftApplication");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();

                Log.i("NAB", "onAdOpened");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();

                Log.i("NAB", "onAdLoaded");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!mInterstitialAd.isLoaded() && !mInterstitialAd.isLoading()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);

            Log.i("NAB", "Trying to load again");
        }
    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet connection.");
        builder.setMessage("You have no internet connection");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        return builder;
    }

    public void OnClickWhatArePokMonButton(View view) {
        NavigateToWebView("http://www.ginnypix.com/pokeguide/what_are_pokemon.html");
    }

    public void OnClickPokMonVideoGamesButton(View view) {
        NavigateToWebView("http://www.ginnypix.com/pokeguide/pokemon_video_games.html");
    }

    public void OnClickPokMonTradingCardButton(View view) {
        NavigateToWebView("http://www.ginnypix.com/pokeguide/pokemon_trading_card_game.html");
    }

    public void OnClickPokMonAnimationButton(View view) {
        NavigateToWebView("http://www.ginnypix.com/pokeguide/pokemon_animation.html");
    }

    public void OnClickPlayPokMon(View view) {
        NavigateToWebView("http://www.ginnypix.com/pokeguide/play_pokemon.html");
    }

    void NavigateToWebView(String url) {
        intent = new Intent(getBaseContext(), WebViewActivity.class);
        intent.putExtra(WebViewActivity.EXTRA_URL, url);
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            startActivity(intent);
        }
    }
}
